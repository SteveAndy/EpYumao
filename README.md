## 功能介绍 
    
 羽毛球馆预订小程序，是一款专门为羽毛球馆从业者设计的小程序平台， 包括本店动态，运动常识，场地预约等功能。 
可以随时查看当天场地的预约数量，随时掌握自己场地的经营情况。 
对于羽毛球爱好者来说，可以实时查看场地数据，一目了然查看还有多少场地可以预定， 按自己的时段安排来合理打球时间。
 
![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

## 技术运用
- 本项目使用微信小程序平台进行开发。
- 使用腾讯专门的小程序云开发技术，云资源包含云函数，数据库，带宽，存储空间，定时器等，资源配额价格低廉，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合快速开发迭代。
- 云开发技术采用腾讯内部链路，没有被黑客攻击的风险，安全性高且免维护。
- 资源承载力可根据业务发展需要随时弹性扩展。  



## 作者
- 如有疑问，欢迎骚扰联系我鸭：开发交流，技术分享，问题答疑，功能建议收集，版本更新通知，安装部署协助，小程序开发定制等。
- 俺的微信:
 
![输入图片说明](demo/author-base.png)


## 演示
![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
 

## 安装

- 安装手册见源码包里的word文档




## 截图

![输入图片说明](demo/1%E9%A6%96%E9%A1%B5.png)
![输入图片说明](demo/2%E6%9C%AC%E9%A6%86%E5%8A%A8%E6%80%81.png)
![输入图片说明](demo/3%E8%BF%90%E5%8A%A8%E5%B8%B8%E8%AF%86.png)
![输入图片说明](demo/4%E9%A2%84%E7%BA%A6%E6%97%A5%E5%8E%86.png)
 ![输入图片说明](demo/5%E6%88%91%E7%9A%84.png)
![输入图片说明](demo/6%E5%9C%BA%E5%9C%B0%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/7%E6%8F%90%E4%BA%A4%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/8%E9%A2%84%E7%BA%A6%E6%88%90%E5%8A%9F.png)
## 后台管理系统截图
![输入图片说明](demo/10%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/11%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E8%8F%9C%E5%8D%95.png)
![输入图片说明](demo/12%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E5%90%8D%E5%8D%95.png)
 ![输入图片说明](demo/13%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E5%90%8D%E5%8D%95%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/14-%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E5%AF%BC%E5%87%BA.png)

![输入图片说明](demo/15-%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E5%90%8D%E5%8D%95%E5%AF%BC%E5%87%BA.png)